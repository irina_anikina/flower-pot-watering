import React from 'react';
import { waterOutline, bugOutline, rainyOutline } from 'ionicons/icons';
import { IonIcon } from '@ionic/react';
import './FlowerPotsKeyFeatures.scss';

export default function FlowerPotsKeyFeatures(): JSX.Element {
  return (
    <section className="flower-pots-key-features">
      <div className="flower-feature">
        <IonIcon icon={waterOutline} className="flower-feature__icon" />
        <h3 className="flower-feature__header">Watering</h3>
        <p className="flower-feature__content">
          An essential action will be done
          {' '}
          <br />
          {' '}
          with the help of our reminders.
        </p>
      </div>
      <div className="flower-feature">
        <IonIcon icon={rainyOutline} className="flower-feature__icon" />
        <h3 className="flower-feature__header">Hydration</h3>
        <p className="flower-feature__content">
          Flower like a human,
          {' '}
          <br />
          {' '}
          it drinks 6 — 8 glasses of water per day.
        </p>
      </div>
      <div className="flower-feature">
        <IonIcon icon={bugOutline} className="flower-feature__icon" />
        <h3 className="flower-feature__header">Fertiliser</h3>
        <p className="flower-feature__content">
          For any creature it is vital
          {' '}
          <br />
          {' '}
          to get nutrition. We take care of it.
        </p>
      </div>
    </section>
  );
}
